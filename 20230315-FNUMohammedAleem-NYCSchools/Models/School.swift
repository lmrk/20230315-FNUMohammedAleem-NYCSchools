//
//  School.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 14/03/23.
//

import Foundation

struct School: Identifiable, Codable {
    var id: String = UUID().uuidString
    var dbn: String?
    var name: String?
    var overview: String?
    var academicOpportunities1: String?
    var academicOpportunities2: String?
    var academicOpportunities3: String?
    var academicOpportunities4: String?
    var academicOpportunities5: String?
    var extracurricularActivities: String?
    var addressLine1: String?
    var neighborhood: String?
    var city: String?
    var state: String?
    var phone: String?
    var email: String?
    var website: String?
    var latitude: String?
    var longitude: String?
    
    enum Codingkeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case overview = "overview_paragraph"
        case academicOpportunities1 = "academicopportunities1"
        case academicOpportunities2 = "academicopportunities2"
        case academicOpportunities3 = "academicopportunities3"
        case academicOpportunities4 = "academicopportunities4"
        case academicOpportunities5 = "academicopportunities5"
        case extracurricularActivities = "extracurricular_activities"
        case addressLine1 = "primary_address_line_1"
        case neighborhood
        case city
        case state = "state_code"
        case phone = "phone_number"
        case email = "school_email"
        case website
        case latitude
        case longitude
    }
    
    init(from decoder: Decoder) throws {
        let map = try decoder.container(keyedBy: Codingkeys.self)
        dbn = try map.decodeIfPresent(String.self, forKey: .dbn)
        name = try map.decodeIfPresent(String.self, forKey: .name)
        overview = try map.decodeIfPresent(String.self, forKey: .overview)
        academicOpportunities1 = try map.decodeIfPresent(String.self, forKey: .academicOpportunities1)
        academicOpportunities2 = try map.decodeIfPresent(String.self, forKey: .academicOpportunities2)
        academicOpportunities3 = try map.decodeIfPresent(String.self, forKey: .academicOpportunities3)
        academicOpportunities4 = try map.decodeIfPresent(String.self, forKey: .academicOpportunities4)
        academicOpportunities5 = try map.decodeIfPresent(String.self, forKey: .academicOpportunities5)
        extracurricularActivities = try map.decodeIfPresent(String.self, forKey: .extracurricularActivities)
        addressLine1 = try map.decodeIfPresent(String.self, forKey: .addressLine1)
        neighborhood = try map.decodeIfPresent(String.self, forKey: .neighborhood)
        city = try map.decodeIfPresent(String.self, forKey: .city)
        state = try map.decodeIfPresent(String.self, forKey: .state)
        phone = try map.decodeIfPresent(String.self, forKey: .phone)
        email = try map.decodeIfPresent(String.self, forKey: .email)
        website = try map.decodeIfPresent(String.self, forKey: .website)
        latitude = try map.decodeIfPresent(String.self, forKey: .latitude)
        longitude = try map.decodeIfPresent(String.self, forKey: .longitude)
    }
}
