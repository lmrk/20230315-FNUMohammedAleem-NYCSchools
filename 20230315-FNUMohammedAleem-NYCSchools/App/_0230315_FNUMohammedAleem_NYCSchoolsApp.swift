//
//  _0230315_FNUMohammedAleem_NYCSchoolsApp.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 14/03/23.
//

import SwiftUI

@main
struct _0230315_FNUMohammedAleem_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolsListView()
        }
    }
}
