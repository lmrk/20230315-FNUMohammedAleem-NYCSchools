//
//  SchoolService.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 15/03/23.
//

import Foundation

struct SchoolService {
    private init() { }
    static let shared = SchoolService()
    
    private let baseURL = "https://data.cityofnewyork.us/resource"
    private let schoolDataEndpoint = "/s3k6-pzi2"
    private let satResulstsEndpoint = "/f9bf-2cp4"
    
    func getSchoolsData() async throws -> ServerResult<[School]> {
        return try await RequestManager.shared.getData(url: baseURL + schoolDataEndpoint)
    }
    
    func getSATResults() async throws -> ServerResult<[SATResult]> {
        return try await RequestManager.shared.getData(url: baseURL + satResulstsEndpoint)
    }
}
