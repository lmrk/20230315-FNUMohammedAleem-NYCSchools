//
//  SchoolsListView.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 14/03/23.
//

import SwiftUI


// TODO: Need to add localisations for all strings
// TODO: Add Sorting and Grouping functionalites

struct SchoolsListView: View {
    @ObservedObject private var viewModel = SchoolsListViewModel()
    var body: some View {
        NavigationStack {
            VStack {
                if viewModel.loading {
                    ProgressView()
                } else {
                    List(viewModel.filteredSchools) { school in
                        if let dbn = school.dbn, let satResult = viewModel.satResult(dbn: dbn) {
                            SchoolsListCell(school: school, satResult: satResult)
                        }
                    }
                    .searchable(text: $viewModel.searchTerm)
                    .navigationTitle("NYC Schools")
                }
            }
        }
        .onChange(of: viewModel.searchTerm) { searchTerm in
            viewModel.applyFilter(searchTerm: searchTerm)
        }
        .alert(isPresented: $viewModel.showAlert) {
            viewModel.errorAlert
        }
        .task {
            // TODO: Fetching all the data for now. We can create a separate view Model for view details screen and fetch details for only selected school.
            await viewModel.getSchoolsData()
            await viewModel.fetchSATResults()
        }
    }
}
