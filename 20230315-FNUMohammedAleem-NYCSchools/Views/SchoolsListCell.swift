//
//  SchoolsListCell.swift
//  20230315-FNUMohammedAleem-NYCSchools
//
//  Created by Aleem on 15/03/23.
//

import SwiftUI

struct SchoolsListCell: View {
    let school: School
    let satResult: SATResult
    
    var body: some View {
        NavigationLink(
            destination: SchoolDetailsView(school: school, satResult: satResult),
            label: {
                VStack(alignment: .leading) {
                    Text(school.name ?? "")
                        .font(.headline)
                        .padding(.bottom)
                    Text(school.addressLine1 ?? "")
                    Text("\(school.neighborhood ?? ""), \(school.city ?? "")")
                }
            }
        )
    }
}
