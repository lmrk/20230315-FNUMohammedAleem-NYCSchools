//
//  SchoolsListViewModelTests.swift
//  20230315-FNUMohammedAleem-NYCSchoolsTests
//
//  Created by Aleem on 15/03/23.
//

import XCTest
@testable import _0230315_FNUMohammedAleem_NYCSchools

final class SchoolsListViewModelTests: XCTestCase {
    
    @MainActor
    func testFilter() throws {
        let bundle = Bundle(for: type(of: self))
        
        guard let schoolsURL = bundle.url(forResource: "Schools", withExtension: "json") else {
            XCTFail("Missing file: SATResult.json")
            return
        }
        
        guard let satResultsURL = bundle.url(forResource: "SATResults", withExtension: "json") else {
            XCTFail("Missing file: SATResult.json")
            return
        }
        
        let schoolsJSONData = try Data(contentsOf: schoolsURL)
        let satResultsJSONData = try Data(contentsOf: satResultsURL)
        
        let schools = try JSONDecoder().decode([School].self, from: schoolsJSONData)
        let satResults = try JSONDecoder().decode([SATResult].self, from: satResultsJSONData)
        let viewModel = SchoolsListViewModel()
        viewModel.schools = schools
        viewModel.satResults = satResults
        
        let satResult = viewModel.satResult(dbn: "01M292")
        XCTAssertEqual(satResult?.dbn, "01M292")
        XCTAssertEqual(satResult?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssertEqual(satResult?.reading, "355")
        XCTAssertEqual(satResult?.math, "404")
        XCTAssertEqual(satResult?.writing, "363")
        XCTAssertEqual(satResult?.testTakers, "29")
        
        viewModel.applyFilter(searchTerm: "Manhattan")
        XCTAssertEqual(viewModel.filteredSchools.count, 109)
    }
}
